package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.AirSituation.AirSituationProvider;
import iaf.ofek.hadracha.base_course.web_server.AirSituation.Airplane;
import iaf.ofek.hadracha.base_course.web_server.Data.InMemoryMapDataBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/ejectedPilotRescue/infos")
public class EjectionsController {

    private InMemoryMapDataBase db;

    public EjectionsController( @Autowired InMemoryMapDataBase db) {
        this.db = db;
    }

    @GetMapping
    public List<EjectedPilotInfo> getEjections(){
        return db.getAllOfType(EjectedPilotInfo.class);
    }
}
