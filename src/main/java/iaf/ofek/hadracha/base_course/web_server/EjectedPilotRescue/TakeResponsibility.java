package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.InMemoryMapDataBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ejectedPilotRescue/takeResponsibility")
public class TakeResponsibility {
    private InMemoryMapDataBase db;

    public TakeResponsibility(@Autowired InMemoryMapDataBase db) {
        this.db = db;
    }

    @GetMapping
    public String getEjections(@RequestParam String ejectionId, @CookieValue(value = "client-id", defaultValue = "") String clientId) {
        EjectedPilotInfo info = db.getByID(Integer.parseInt(ejectionId),EjectedPilotInfo.class);
        info.setRescuedBy(clientId);
        db.update(info);
        return ejectionId;
    }
}
